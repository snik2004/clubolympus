package com.snik2004.clubolympus;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import static com.snik2004.clubolympus.data.ClubOlympusContract.memberEntry.*;

public class OlympusCursorAdapter extends CursorAdapter {

    public OlympusCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.activity_list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView tvFirstName = view.findViewById(R.id.firsNameTextView);
        TextView tvLastName = view.findViewById(R.id.lastNameTextView);
        TextView tvSport = view.findViewById(R.id.sportTextView);

        String fitstName = cursor.getString(cursor.getColumnIndexOrThrow(KEY_FIRST_NAME));
        String lastName = cursor.getString(cursor.getColumnIndexOrThrow(KEY_LAST_NAME));
        String sport = cursor.getString(cursor.getColumnIndexOrThrow(KEY_SPORT));

        tvFirstName.setText(fitstName);
        tvLastName.setText(lastName);
        tvSport.setText(sport);

    }
}
