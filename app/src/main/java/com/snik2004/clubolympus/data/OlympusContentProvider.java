package com.snik2004.clubolympus.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import static com.snik2004.clubolympus.data.ClubOlympusContract.AUTHORITY;
import static com.snik2004.clubolympus.data.ClubOlympusContract.PATH_MEMBERS;
import static com.snik2004.clubolympus.data.ClubOlympusContract.memberEntry.CONTENT_MULTIPLE_ITEMS;
import static com.snik2004.clubolympus.data.ClubOlympusContract.memberEntry.CONTENT_SINGLE_ITEM;
import static com.snik2004.clubolympus.data.ClubOlympusContract.memberEntry.GENDER_FEMALE;
import static com.snik2004.clubolympus.data.ClubOlympusContract.memberEntry.GENDER_MALE;
import static com.snik2004.clubolympus.data.ClubOlympusContract.memberEntry.GENDER_UNKLOWN;
import static com.snik2004.clubolympus.data.ClubOlympusContract.memberEntry.KEY_FIRST_NAME;
import static com.snik2004.clubolympus.data.ClubOlympusContract.memberEntry.KEY_GENDER;
import static com.snik2004.clubolympus.data.ClubOlympusContract.memberEntry.KEY_ID;
import static com.snik2004.clubolympus.data.ClubOlympusContract.memberEntry.KEY_LAST_NAME;
import static com.snik2004.clubolympus.data.ClubOlympusContract.memberEntry.KEY_SPORT;
import static com.snik2004.clubolympus.data.ClubOlympusContract.memberEntry.TABLE_NAME;


public class OlympusContentProvider extends ContentProvider {

    OlympusDBOpenHelper dbOpenHelper;
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private static final int MEMBERS = 111;
    private static final int MEMBER_ID = 222;

    static {

        uriMatcher.addURI(AUTHORITY, PATH_MEMBERS, MEMBERS);
        uriMatcher.addURI(AUTHORITY, PATH_MEMBERS + "/#", MEMBER_ID);
    }

    @Override
    public boolean onCreate() {
        dbOpenHelper = new OlympusDBOpenHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
        Cursor cursor;
        int match = uriMatcher.match(uri);
        switch (match) {
            case MEMBERS:
                cursor = db.query(TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case MEMBER_ID:
                selection = KEY_ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = db.query(TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                Toast.makeText(getContext(), "Incorrect URI", Toast.LENGTH_LONG).show();
                throw new IllegalArgumentException("Can't query incorrect URI " + uri);

        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String firstName = values.getAsString(KEY_FIRST_NAME);
        if (firstName == null) {
            throw new IllegalArgumentException("You have to input firstname");
        }
        String lastName = values.getAsString(KEY_LAST_NAME);
        if (lastName == null) {
            throw new IllegalArgumentException("You have to input lastName");
        }
        Integer gender = values.getAsInteger(KEY_GENDER);
        if (gender == null || !(gender == GENDER_UNKLOWN || gender == GENDER_FEMALE || gender == GENDER_MALE)) {
            throw new IllegalArgumentException("You have to input gender");
        }
        String sport = values.getAsString(KEY_SPORT);
        if (sport == null) {
            throw new IllegalArgumentException("You have to input sport");
        }

        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        int match = uriMatcher.match(uri);
        switch (match) {
            case MEMBERS:
                long id = db.insert(TABLE_NAME, null, values);
                if (id == -1) {
                    Log.e("insert methot", "Insertion of data in the table failed for " + uri);
                    return null;
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return ContentUris.withAppendedId(uri, id);

            default:
                throw new IllegalArgumentException("Insertion of data in the table failed for " + uri);

        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        int match = uriMatcher.match(uri);
        int rowsDeleted;
        switch (match) {
            case MEMBERS:
                rowsDeleted = db.delete(TABLE_NAME, selection, selectionArgs);
                break;
            case MEMBER_ID:
                selection = KEY_ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsDeleted = db.delete(TABLE_NAME, selection, selectionArgs);
                break;
            default:
                Toast.makeText(getContext(), "Incorrect URI", Toast.LENGTH_LONG).show();
                throw new IllegalArgumentException("Can't delete incorrect URI " + uri);
        }
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (values.containsKey(KEY_FIRST_NAME)) {

        }
        String firstName = values.getAsString(KEY_FIRST_NAME);
        if (firstName == null) {
            throw new IllegalArgumentException("You have to input firstname");
        }
        if (values.containsKey(KEY_LAST_NAME)) {
            String lastName = values.getAsString(KEY_LAST_NAME);
            if (lastName == null) {
                throw new IllegalArgumentException("You have to input lastName");
            }
        }

        if (values.containsKey(KEY_GENDER)) {
            Integer gender = values.getAsInteger(KEY_GENDER);
            if (gender == null || !(gender == GENDER_UNKLOWN || gender == GENDER_FEMALE || gender == GENDER_MALE)) {
                throw new IllegalArgumentException("You have to input gender");
            }
        }

        if (values.containsKey(KEY_SPORT)) {
            String sport = values.getAsString(KEY_SPORT);
            if (sport == null) {
                throw new IllegalArgumentException("You have to input sport");
            }
        }

        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        int match = uriMatcher.match(uri);
        int rowsUpdated;
        switch (match) {
            case MEMBERS:
                rowsUpdated = db.update(TABLE_NAME, values, selection, selectionArgs);
                break;
            case MEMBER_ID:
                selection = KEY_ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsUpdated = db.update(TABLE_NAME, values, selection, selectionArgs);
                break;

            default:
                Toast.makeText(getContext(), "Incorrect URI", Toast.LENGTH_LONG).show();
                throw new IllegalArgumentException("Can't update incorrect URI " + uri);

        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);

        }
        return rowsUpdated;
    }

    @Override
    public String getType(Uri uri) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case MEMBERS:
                return CONTENT_MULTIPLE_ITEMS;

            case MEMBER_ID:
                return CONTENT_SINGLE_ITEM;

            default:
                throw new IllegalArgumentException("Unklown URI " + uri);

        }
    }
}
